# Novation Launchpad MK2

A working launchpad mk2 ES6 node.js library. Tested on node.js 14.5.

---

Based on work by https://github.com/tjhorner/node-launchpad-mk2 , which looks like it was based off of https://github.com/Granjow/launchpad-mini.

# Novation Launchapd Mini MK3

Enter 'programming mode' by entering the settings menu. To enter the settings menu, press and hold Session briefly. The top 4 rows will display the characters "LED", indicating the topic of the menu. Push the bottom right button 'stop solo mute' to enter into programmer mode. The button above (a green >) will revert to 'live' mode.