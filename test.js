import Launchpad from './index.js';

var myLaunchpad = new Launchpad();
try { 
  await myLaunchpad.connect();
} catch (e) { 
  console.log(e); 
  process.exit();
}

// myLaunchpad.lightAll(0);
myLaunchpad.scrollText("Hello World", 53, false, 5);

myLaunchpad.on("press", pressInfo => {
  var button = pressInfo.button;
  var velocity = pressInfo.velocity;
  console.log(pressInfo.button)
  button.pulseColor(Math.floor((Math.random() * 127) + 1))
  if (button.x === 1 && button.y === 0) launchpad.darkAll()
})

myLaunchpad.on("release", button => {
  button.setColor(0);
})
